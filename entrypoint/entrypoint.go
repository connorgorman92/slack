package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"

	log "github.com/golang/glog"

	"github.com/connorgorman/slack/server"

	_ "github.com/connorgorman/slack/cache/lru"
	_ "github.com/connorgorman/slack/discovery/zookeeper"
)

// Version is set at build time by ldflags
var Version string

func main() {
	var (
		ip               string
		serviceDiscovery string
		maxConns         int
		cacheSize        uint64
		version          bool
		help             bool
	)
	flag.StringVar(&ip, "ip-address", "", "ip:port address of server")
	flag.Uint64Var(&cacheSize, "cache-size", 1073741824, "size of the cache")
	flag.StringVar(&serviceDiscovery, "service-discovery", "", "comma-separated ips of service discovery in ip:port format")
	flag.IntVar(&maxConns, "max-conns", 65535, "max number of simultaneous tcp connections")
	flag.BoolVar(&version, "version", false, "show version")
	flag.BoolVar(&help, "help", false, "show commandline help")
	flag.Parse()

	if version {
		fmt.Println("Version: " + Version)
	} else if help {
		flag.PrintDefaults()
	} else if ip == "" {
		log.Fatal("flag ip-address must be defined")
	}
	serviceDiscoveryIPs := strings.Split(serviceDiscovery, ",")

	s, err := server.NewServer("lru", "zookeeper", ip, maxConns, cacheSize)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt, os.Kill)
		<-sigChan
		s.Close()
		os.Exit(0)
	}()

	// TODO(cgorman) There's an issue of ordering as it will register in service discovery before accepting connections
	if err := s.RegisterWithServiceDiscovery(serviceDiscoveryIPs); err != nil {
		log.Fatal(err)
	}
	s.Start()
}
