# Slack Memcache challenge #

## Installation (Installed on 14.04)

```
sudo curl -O https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
sudo tar -xvf go1.8.linux-amd64.tar.gz
sudo mv go /usr/local
mkdir -p gopath/src/github.com/connorgorman
export GOPATH=$(pwd)/gopath
cd $GOPATH/src/github.com/connorgorman
git clone https://connorgorman92@bitbucket.org/connorgorman92/slack.git
cd slack
git submodule init
git submodule update
make test
make memcache-server
make memcache-client

wget http://mirror.metrocast.net/apache/zookeeper/zookeeper-3.4.9/zookeeper-3.4.9.tar.gz
tar -xvf zookeeper-3.4.9.tar.gz
cp zoo.cfg zookeeper-3.4.9/conf/zoo.cfg
bash zookeeper-3.4.9/bin/zkServer.sh # This runs zookeeper which is used for service discovery


bin/memcache-server --ip-address=127.0.0.1:6267 -logtostderr=true --service-discovery=127.0.0.1
bin/memcache-client -logtostderr=true
```

## Discussion

I would expect my implementation to be fairly performant despite having locks on the LRU cache. The amount of time spent within the locks is O(1) for everything but "set" which is O(N) as it could need to evict all other elements. Specifying a max object size would help prevent the total eviction of the cache. However, even with the locking structure, if there were a large num of connections on the server then the lock contention could slow down the latency. I have implemented two ways to help combat this issue. I defined a maxium number of connections for a server which will reject any more connections so that the current connections can still receive a current response. I also added a basic service discovery mechanism through zookeeper. The memcache server registers when it starts and each memcache client listens for a new server set. This allows the addition/removal of servers to be fairly lightweight. One caveat of this is that a new added or removed server will decrease the cache hit ratio for a period of time as now the keys hash to different servers. I used a modulo hashing approach, but there are more complex consistent hashing approaches which would reduce the amount of misses in case of server set change.

Performance could be increased depending on the request distribution by using a cache that optimizes for that use case. For example, if some objects are more expensive to retrieve then greedy dual size could be more performant. From a reliability standpoint, a bit more work could go into the parsing of the requests (for sake of time didn't spend of time writing specific errors for request failures)

From a monitoring perspective, I could implement the stats endpoint on the memcache server and also add more stats to the caching layer. These could be pulled by something like Prometheus or Telegraf and pushed to a central monitoring system. 

Testing:
 * Unit testing
 * Wrote example which tested from client perspective
 * Basic load testing scaffolding (not dev complete)