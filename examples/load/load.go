package main

import (
	"flag"
	log "github.com/golang/glog"
	"time"

	"github.com/connorgorman/slack/client"
	_ "github.com/connorgorman/slack/discovery/zookeeper"
)

func main() {
	var (
		numClients    int
		reqs          int
		requestType   string
		numUniqueKeys int
	)
	flag.IntVar(&numClients, "num-client", 1, "number of load test clients")
	flag.IntVar(&reqs, "reqs", 100000, "total requests")
	flag.StringVar(&requestType, "type", "get", "get requests")
	flag.IntVar(&numUniqueKeys, "unique", 20, "unique keys")
	flag.Parse()

	reqChan := make(chan int, numClients)
	start := time.Now()
	for i := 0; i < numClients; i++ {
		go func() {
			memcacheClient, err := client.NewClient("zookeeper")
			if err != nil {
				log.Fatal(err)
			}
			if err := memcacheClient.Initialize([]string{"127.0.0.1"}); err != nil {
				log.Fatal(err)
			}
			for j := 0; j < reqs; j++ {
				memcacheClient.Retrieve(client.GetRequest{
					Keys: []string{"goodbye"},
				})
			}
			reqChan <- 0
		}()
	}
	for i := 0; i < numClients; i++ {
		<-reqChan
	}
	end := time.Since(start)
	log.Infof("Time since start: %v. Processed: %+v requests", end.Seconds(), reqs*numClients)
	log.Flush()
}
