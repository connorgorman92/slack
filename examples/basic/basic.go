package main

import (
	"flag"
	log "github.com/golang/glog"

	"github.com/connorgorman/slack/client"
	_ "github.com/connorgorman/slack/discovery"
	_ "github.com/connorgorman/slack/discovery/zookeeper"
)

func main() {
	flag.Parse()
	memcacheClient, err := client.NewClient("zookeeper")
	if err != nil {
		log.Fatal(err)
	}
	if err := memcacheClient.Initialize([]string{"127.0.0.1"}); err != nil {
		log.Fatal(err)
	}
	values, _ := memcacheClient.Retrieve(client.GetRequest{
		Keys: []string{"goodbye"},
	})
	log.Infof("Retrieved Values")
	for _, value := range values {
		log.Infof("%+v", value)
	}
	sr := &client.SetRequest{
		Key:     "goodbye",
		Flags:   5,
		Bytes:   7,
		Data:    "goodbye",
		NoReply: false,
	}
	if err := memcacheClient.Store(sr); err != nil {
		log.Fatal(err)
	}
	log.Infof("Set goodbye")
	values, _ = memcacheClient.Retrieve(client.GetRequest{
		Keys: []string{"goodbye"},
	})
	log.Infof("Retrieved Values")
	for _, value := range values {
		log.Infof("%+v", value)
	}
	sr.Bytes = 5
	sr.Data = "adios"
	cr := &client.CASRequest{
		SetRequest: sr,
		CASValue:   values[0].CASValue,
	}
	if err := memcacheClient.Store(cr); err != nil {
		log.Fatal(err)
	}
	log.Infof("CAS request complete")
	values, _ = memcacheClient.Retrieve(client.GetRequest{
		Keys: []string{"goodbye"},
	})
	log.Infof("Retrieved Values")
	for _, value := range values {
		log.Infof("%+v", value)
	}
	if err := memcacheClient.Store(cr); err != nil {
		log.Infof("CAS request failure: %+v", err)
	}
}
