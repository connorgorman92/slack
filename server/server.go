package server

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"

	log "github.com/golang/glog"
	"github.com/satori/go.uuid"
	"golang.org/x/net/netutil"

	"github.com/connorgorman/slack/cache"
	"github.com/connorgorman/slack/discovery"
)

// serverError returns a string formatted in memcache server error
func serverError(msg string) string {
	return fmt.Sprintf("SERVER_ERROR %v\r\n", msg)
}

// clientError returns a string formatted in memcache client error
func clientError(msg string) string {
	return fmt.Sprintf("CLIENT_ERROR %v\r\n", msg)
}

// Stats keeps track of the stats of the memcache server
// TODO(cgorman) Expand upon these stats
type Stats struct {
	NumConnections int64
}

// MemcacheElement is the element actually stored in the cache
type MemcacheElement struct {
	Flags    uint32
	Value    string
	CASValue uint64
	Bytes    uint64
}

func (me *MemcacheElement) toRetrievalResponse(key string, cas bool) string {
	return fmt.Sprintf("VALUE %s %d %d %d\r\n%s\r\n", key, me.Flags, len(me.Value), me.CASValue, me.Value)
}

// Server defines the memcache server
type Server struct {
	lock     sync.RWMutex // lock is used to make actions against the cache thread-safe
	memcache cache.Cache  // memcache is the backing cache

	serviceDiscovery discovery.ServiceDiscovery // service discovery makes the server discoverable by clients

	ip       string       // ip is the ip address of the server in ip:port format
	maxConns int          // maxConns is the max connections this server can have simultaneously
	listener net.Listener //listener listens for connections from clients
}

// NewServer takes in cacheType, serviceDiscoveryName, ip, maxConns and cacheSize and returns a new Server
func NewServer(cacheType string, serviceDiscoveryName string, ip string, maxConns int, cacheSize uint64) (*Server, error) {
	s := &Server{
		maxConns: maxConns,
		ip:       ip,
	}
	serviceDiscovery, ok := discovery.ServiceDiscoveryRegistry[serviceDiscoveryName]
	if !ok {
		return nil, fmt.Errorf("Registration service %s does not exist", serviceDiscoveryName)
	}
	s.serviceDiscovery = serviceDiscovery
	cacheMechanism, ok := cache.Caches[cacheType]
	if !ok {
		return nil, fmt.Errorf("Cache type %s does not exist", cacheType)
	}
	cacheMechanism.SetSize(cacheSize)
	s.memcache = cacheMechanism
	return s, nil
}

// RegisterWithServiceDiscovery registers this server with it's service discovery implementation
func (s *Server) RegisterWithServiceDiscovery(serviceDiscoveryIPs []string) error {
	err := s.serviceDiscovery.Register(serviceDiscoveryIPs, s.ip)
	return err
}

// Close closes the servers connections
func (s *Server) Close() {
	if err := s.listener.Close(); err != nil {
		log.Errorf("Error trying to close listener: %+v", err)
	}
}

// Start starts the tcp listener and handles requests
func (s *Server) Start() {
	log.V(3).Infof("Listening on %s", s.ip)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", s.ip)
	if err != nil {
		log.Fatalf("Failed to bind to ip=%v. Error=%+v", s.ip, err)
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		log.Fatalf("Failed to listen on ip=%v. Error=%+v", s.ip, err)
	}

	s.listener = netutil.LimitListener(listener, s.maxConns)

	for {
		conn, err := s.listener.Accept()
		log.V(3).Infof("Accepted connection on %s", s.ip)
		if err != nil {
			log.Fatalf("Error accepting connection: %+v", err)
		}
		go s.handleClient(conn)
	}
}

// retrieveRequest is the endpoint for a get/gets request
func (s *Server) retrieveRequest(conn net.Conn, splitLine ...string) {
	var returnStr string
	defer func() {
		conn.Write([]byte(returnStr))
	}()
	// Check for malform request
	if len(splitLine) == 0 {
		returnStr = clientError("malformed retrieveal request")
		return
	}
	for _, key := range splitLine {
		val, err := s.get(key)
		if err == cache.ErrKeyNotFound { // Value not found so continue
			continue
		}
		returnStr += val.toRetrievalResponse(key, false)
	}
	returnStr += "END\r\n"
}

// parseFirst4StorageArgs parses the arguments that are common to all storage requests
func parseFirst4StorageArgs(conn net.Conn, splitLine ...string) (string, uint32, uint32, error) {
	key := splitLine[0]
	flags64, err := strconv.ParseInt(splitLine[1], 10, 32)
	if err != nil {
		return "", 0, 0, err
	}
	flags := uint32(flags64)
	// splitLine[2] for set is exptime which we are not handling for this exercise
	bytes64, err := strconv.ParseInt(splitLine[3], 10, 32)
	if err != nil {
		return "", 0, 0, err
	}
	bytes := uint32(bytes64)
	return key, flags, bytes, nil
}

// storageRequest is the endpoint for a set request
func (s *Server) storageRequest(reader *bufio.Reader, conn net.Conn, splitLine ...string) {
	var noReply bool
	var returnStr string
	defer func() {
		if !noReply {
			conn.Write([]byte(returnStr))
		}
	}()
	if len(splitLine) < 4 || len(splitLine) > 5 {
		returnStr = clientError("malformed storage request")
		return
	}
	noReply = len(splitLine) == 5 && splitLine[4] == "noreply"

	key, flags, bytes, err := parseFirst4StorageArgs(conn, splitLine...)
	if err != nil {
		returnStr = clientError(err.Error())
		return
	}

	line, _, err := reader.ReadLine()
	if err != nil {
		returnStr = clientError(err.Error())
		return
	}
	// Verify the bytes read are the same as expected
	if len(line) != int(bytes) {
		returnStr = clientError("Bytes must match the size of the data")
		return
	}
	if err := s.set(key, &MemcacheElement{Flags: flags, Value: string(line), Bytes: uint64(bytes)}); err != nil {
		returnStr = serverError(err.Error())
	}
	returnStr = Stored.String()
}

// deleteRequest is the endpoint for a delete request
func (s *Server) deleteRequest(conn net.Conn, splitLine ...string) {
	var noReply bool
	var returnStr string
	defer func() {
		if !noReply {
			conn.Write([]byte(returnStr))
		}
	}()
	// Check for malformed request
	if len(splitLine) < 1 || len(splitLine) > 2 {
		returnStr = clientError("malformed delete request")
		return
	}
	key := splitLine[0]
	noReply = len(splitLine) == 2 && splitLine[1] == "noreply"
	if err := s.delete(key); err == cache.ErrKeyNotFound {
		returnStr = NotFound.String()
	} else if err != nil {
		returnStr = serverError(fmt.Sprintf("Error deleting value: %+v", err.Error()))
	} else {
		returnStr = Deleted.String()
	}
}

// casRequest is the endpoint for a cas request
func (s *Server) casRequest(reader *bufio.Reader, conn net.Conn, splitLine ...string) {
	var noReply bool
	var returnStr string
	defer func() {
		if !noReply {
			conn.Write([]byte(returnStr))
		}
	}()
	if len(splitLine) < 5 || len(splitLine) > 6 {
		returnStr = clientError("malform cas request")
		return
	}
	noReply = len(splitLine) == 6 && splitLine[5] == "noreply"
	key, flags, bytes, err := parseFirst4StorageArgs(conn, splitLine...)
	if err != nil {
		returnStr = clientError(err.Error())
		return
	}
	casID64, err := strconv.ParseInt(splitLine[4], 10, 64)
	if err != nil {
		returnStr = clientError(err.Error())
		return
	}
	casID := uint64(casID64)
	line, _, err := reader.ReadLine()
	if err != nil {
		returnStr = clientError(err.Error())
		return
	}
	// Verify the bytes read are the same as expected
	if len(line) != int(bytes) {
		returnStr = clientError("Bytes must match the size of the data")
		return
	}
	if err := s.cas(key, casID, &MemcacheElement{Flags: flags, Value: string(line), Bytes: uint64(bytes)}); err == cache.ErrKeyNotFound {
		returnStr = NotFound.String()
		return
	} else if err == cache.ErrUnexpectedValue {
		returnStr = Exists.String()
		return
	} else if err != nil {
		returnStr = serverError(err.Error())
		return
	}
	returnStr = Stored.String()
}

// set generates a CASValue and adds the object into the cache
func (s *Server) set(key string, value *MemcacheElement) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	value.CASValue = binary.BigEndian.Uint64([]byte(uuid.NewV4().String()))
	err := s.memcache.Add(key, value, value.Bytes)
	return err
}

// get retrieves an object from the cache if it exists
// if it does not exist then it returns ErrKeyNotFound
func (s *Server) get(key string) (*MemcacheElement, error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	val, err := s.memcache.Get(key)
	if err != nil {
		return nil, err
	}
	return val.(*MemcacheElement), err
}

// delete removes an element from the cache if it exists
// if it does not exist then it returns ErrKeyNotFound
func (s *Server) delete(key string) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	err := s.memcache.Delete(key)
	return err
}

// cas compares the CASValue of the object in the cache if it exists
// if it does not exist then it returns ErrKeyNotFound
// if the cas value matches then it will generate a new cas value and update the value in the cache
// if the value does not match then it will return ErrUnexpectedValue
func (s *Server) cas(key string, cas uint64, value *MemcacheElement) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	elem, err := s.memcache.Get(key)
	if err != nil {
		return err
	}
	if elem.(*MemcacheElement).CASValue == cas {
		value.CASValue = binary.BigEndian.Uint64([]byte(uuid.NewV4().String()))
		err := s.memcache.Add(key, value, value.Bytes)
		return err
	}
	return cache.ErrUnexpectedValue
}

// handleClient takes in a connection as a parameter and tries to parse the type of request
// if it can parse the request then it calls the appropriate action
func (s *Server) handleClient(conn net.Conn) {
	reader := bufio.NewReader(conn)
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			log.Warningf("Request from client is invalid. Closing connection: %+v", err)
			if err := conn.Close(); err != nil { // purposefully not handling the error here
				log.Errorf("Connection not closed properly: %+v", err)
			}
			return
		}
		log.V(3).Infof("Received Line: %s", line)
		reqSplit := strings.Split(strings.TrimSpace(string(line)), " ")
		if len(reqSplit) <= 1 {
			returnStr := clientError("malformed request")
			conn.Write([]byte(returnStr))
			continue
		}

		switch reqSplit[0] {
		case "get", "gets":
			s.retrieveRequest(conn, reqSplit[1:]...)
		case "set":
			s.storageRequest(reader, conn, reqSplit[1:]...) // Set needs the reader to read the rest of the data
		case "cas":
			s.casRequest(reader, conn, reqSplit[1:]...)
		case "delete":
			s.deleteRequest(conn, reqSplit[1:]...)
		case "stats":
			//TODO(cgorman) implement stats for monitoring
		}
	}
}
