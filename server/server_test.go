package server

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/connorgorman/slack/cache"
	_ "github.com/connorgorman/slack/cache/lru"
	_ "github.com/connorgorman/slack/discovery/zookeeper"
)

func NewTestServer() *Server {
	s, _ := NewServer("lru", "zookeeper", "ip", 10, 1073741824)
	return s
}

type MockConn struct {
	Written string
}

func (mc *MockConn) Read(b []byte) (n int, err error) {
	return 0, nil
}
func (mc *MockConn) Write(b []byte) (n int, err error) {
	mc.Written += string(b)
	return len(b), nil
}
func (mc *MockConn) Close() error                       { return nil }
func (mc *MockConn) LocalAddr() net.Addr                { return nil }
func (mc *MockConn) RemoteAddr() net.Addr               { return nil }
func (mc *MockConn) SetDeadline(t time.Time) error      { return nil }
func (mc *MockConn) SetReadDeadline(t time.Time) error  { return nil }
func (mc *MockConn) SetWriteDeadline(t time.Time) error { return nil }
func (mc *MockConn) Clear() {
	mc.Written = ""
}

func NewMockReader(s string) *bufio.Reader {
	reader := strings.NewReader(s)
	return bufio.NewReader(reader)
}

func TestRetrieveRequest(t *testing.T) {
	conn := &MockConn{}
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	testServer.retrieveRequest(conn)
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// Should return empty response
	testServer.retrieveRequest(conn, "key")
	assert.Equal(t, conn.Written, "END\r\n")
	conn.Clear()

	testServer.set("key1", &MemcacheElement{Value: "value1"})
	testServer.retrieveRequest(conn, "key1")
	assert.Contains(t, conn.Written, "VALUE key1 0 6", "value1", "END\r\n")
	conn.Clear()

	testServer.set("key2", &MemcacheElement{Value: "value2"})
	testServer.set("key3", &MemcacheElement{Value: "value3"})
	testServer.retrieveRequest(conn, "key1", "key2", "key4", "key3")
	assert.Contains(t, conn.Written, "VALUE key1 0 6", "value1", "VALUE key2 0 6", "value2", "VALUE key3 0 6", "value3", "END\r\n")
	assert.NotContains(t, conn.Written, "key4")
	conn.Clear()
}

func TestStorageRequest(t *testing.T) {
	conn := &MockConn{}
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	// Test malformed requests
	// Suppose to be <key> <flags> <exptime> <bytes> <noreply>
	testServer.storageRequest(nil, conn, "", "", "")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	testServer.storageRequest(nil, conn, "", "", "", "", "", "")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable flags
	testServer.storageRequest(nil, conn, "key", "flags", "exptime", "5")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable flags with noreply
	testServer.storageRequest(nil, conn, "key", "flags", "exptime", "5", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// non parseable bytes
	testServer.storageRequest(nil, conn, "key", "10", "exptime", "bytes")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable bytes with noreply
	testServer.storageRequest(nil, conn, "key", "10", "exptime", "bytes", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// Datasize does not match bytes
	reader := NewMockReader("helloworld\r\n")
	testServer.storageRequest(reader, conn, "key", "10", "exptime", "5")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// Valid request
	reader = NewMockReader("helloworld\r\n")
	testServer.storageRequest(reader, conn, "key", "10", "exptime", "10")
	assert.Equal(t, conn.Written, Stored.String())
	conn.Clear()

	// Make sure noreply
	reader = NewMockReader("helloworld\r\n")
	testServer.storageRequest(reader, conn, "key", "10", "exptime", "5", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()
}

func TestDeleteRequest(t *testing.T) {
	conn := &MockConn{}
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	// Test malformed requests
	testServer.deleteRequest(conn)
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	testServer.deleteRequest(conn, "", "", "")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// Verify that even in failure, that nothing is written to the connection
	testServer.deleteRequest(conn, "key-that-doesnt-exist")
	assert.Equal(t, conn.Written, NotFound.String())
	conn.Clear()

	// Verify that even in failure, that nothing is written to the connection
	testServer.deleteRequest(conn, "key-that-doesnt-exist", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// Verify successful deletion
	testServer.set("key", &MemcacheElement{Value: "value"})
	testServer.deleteRequest(conn, "key")
	assert.Equal(t, conn.Written, Deleted.String())
	conn.Clear()

	// Verify successful deletion and noreply
	testServer.set("key", &MemcacheElement{Value: "value"})
	testServer.deleteRequest(conn, "key", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()
}

func TestCASRequest(t *testing.T) {
	conn := &MockConn{}
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	// Test malformed requests
	// Suppose to be <key> <flags> <exptime> <bytes> <noreply>
	testServer.casRequest(nil, conn, "", "", "", "")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	testServer.casRequest(nil, conn, "", "", "", "", "", "")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable flags
	testServer.casRequest(nil, conn, "key", "flags", "exptime", "5", "cas")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable flags with noreply
	testServer.casRequest(nil, conn, "key", "flags", "exptime", "5", "cas", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// non parseable bytes
	testServer.casRequest(nil, conn, "key", "10", "exptime", "bytes", "cas")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable bytes with noreply
	testServer.casRequest(nil, conn, "key", "10", "exptime", "bytes", "cas", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// non parseable cas
	testServer.casRequest(nil, conn, "key", "10", "exptime", "5", "cas")
	assert.Contains(t, conn.Written, ClientError.String())
	conn.Clear()

	// non parseable cas with noreply
	testServer.casRequest(nil, conn, "key", "10", "exptime", "5", "cas", "noreply")
	assert.Equal(t, conn.Written, "")
	conn.Clear()

	// Datasize does not match bytes
	reader := NewMockReader("helloworld\r\n")
	testServer.casRequest(reader, conn, "key", "10", "exptime", "10", "1234")
	assert.Contains(t, conn.Written, NotFound.String())
	conn.Clear()

	// Getting key to see CASValue
	testServer.set("key", &MemcacheElement{Value: "value"})
	elem, err := testServer.get("key")
	assert.Nil(t, err)
	assert.NotNil(t, elem)

	// Try with wrong cas value
	reader = NewMockReader("helloworld\r\n")
	testServer.casRequest(reader, conn, "key", "10", "exptime", "10", fmt.Sprintf("%d", elem.CASValue+1))
	assert.Equal(t, conn.Written, Exists.String())
	conn.Clear()

	// Try with correct cas value
	reader = NewMockReader("worldhello\r\n")
	testServer.casRequest(reader, conn, "key", "10", "exptime", "10", fmt.Sprintf("%d", elem.CASValue))
	assert.Equal(t, conn.Written, Stored.String())
	elem, err = testServer.get("key")
	assert.Nil(t, err)
	assert.NotNil(t, elem)
	assert.Equal(t, "worldhello", elem.Value)
	conn.Clear()
}

func TestGet(t *testing.T) {
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	// Test error if no key
	elem, err := testServer.get("key")
	assert.Equal(t, err, cache.ErrKeyNotFound)
	assert.Nil(t, elem)

	// Make sure get returns correctly
	testElem := &MemcacheElement{Value: "value"}
	testServer.set("key", testElem)
	elem, err = testServer.get("key")
	assert.Nil(t, err)
	assert.Equal(t, testElem, elem)
}

func TestSet(t *testing.T) {
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	err := testServer.set("key", &MemcacheElement{Value: "value"})
	assert.Nil(t, err)

	elem, err := testServer.get("key")
	assert.Nil(t, err)
	assert.NotNil(t, elem)
	assert.Equal(t, "value", elem.Value)
	assert.NotEqual(t, elem.CASValue, 0)
}

func TestDelete(t *testing.T) {
	testServer := NewTestServer()
	defer testServer.memcache.EvictAll()

	err := testServer.set("key", &MemcacheElement{Value: "value"})
	assert.Nil(t, err)

	elem, err := testServer.get("key")
	assert.Nil(t, err)
	assert.NotNil(t, elem)

	err = testServer.delete("key")
	assert.Nil(t, err)

	// Test error if no key
	elem, err = testServer.get("key")
	assert.Equal(t, err, cache.ErrKeyNotFound)
	assert.Nil(t, elem)

	err = testServer.delete("key")
	assert.Equal(t, cache.ErrKeyNotFound, err)
}

func TestCAS(t *testing.T) {
	testServer := NewTestServer()
	err := testServer.cas("key", 0, &MemcacheElement{})
	assert.Equal(t, cache.ErrKeyNotFound, err)

	// Use memcache.Set because .Set will change the CASValue
	testServer.memcache.Add("key", &MemcacheElement{Value: "value", CASValue: 1234}, 5)
	err = testServer.cas("key", 0, &MemcacheElement{})
	assert.Equal(t, cache.ErrUnexpectedValue, err)

	err = testServer.cas("key", 1234, &MemcacheElement{Value: "hiya"})
	assert.Nil(t, err)

	elem, err := testServer.get("key")
	assert.Nil(t, err)
	assert.Equal(t, elem.Value, "hiya")
	assert.NotEqual(t, elem.CASValue, 1234)
}
