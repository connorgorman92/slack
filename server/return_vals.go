package server

// ReturnCode defines all of the acceptable returns from the server
type ReturnCode int

const (
	// Stored indicates successful storage of data
	Stored ReturnCode = iota
	// NotStored indicates that the object was unable to be stored
	NotStored
	// NotFound indicates that the object was not found in the cache
	NotFound
	// Exists indicates that the CAS signature did not match
	Exists
	// Deleted indicates that the entry was removed from the cache
	Deleted
	// ClientError indicates an error in the request format
	ClientError
	// ServerError indicates a server error
	ServerError
)

// String converts a ReturnCode to it's string format
func (r ReturnCode) String() string {
	switch r {
	case Stored:
		return "STORED\r\n"
	case NotStored:
		return "NOT_STORED\r\n"
	case Exists:
		return "EXISTS\r\n"
	case NotFound:
		return "NOT_FOUND\r\n"
	case Deleted:
		return "DELETED\r\n"
	case ClientError:
		return "CLIENT_ERROR" // No endline due because they should be a message after
	case ServerError:
		return "SERVER_ERROR" // No endline due because they should be a message after
	default:
		return "UNKNOWN RETURN VALUE"
	}
}
