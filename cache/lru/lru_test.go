package cache

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/connorgorman/slack/cache"
)

var lru *LRUCache

func TestMain(m *testing.M) {
	lru = NewLRUCache()
	code := m.Run()
	os.Exit(code)
}

func TestGet(t *testing.T) {
	defer lru.EvictAll()

	// Verify miss
	val, err := lru.Get("key1")
	assert.Equal(t, cache.ErrKeyNotFound, err)
	assert.Nil(t, val)
	assert.Equal(t, int64(1), lru.GetStats().Misses)

	// Verify hit
	lru.Add("key1", 1, 4)
	val, err = lru.Get("key1")
	assert.Nil(t, err)
	assert.NotNil(t, val)
	assert.Equal(t, 1, val.(int))
	assert.Equal(t, int64(1), lru.GetStats().Hits)

	// Verify actual LRU ordering
	// By adding them all and then accessing them in opposite order, they should be ordered from highest to lowest
	keys := []string{"key1", "key2", "key3"}
	values := []int{1, 2, 3}
	for idx, key := range keys {
		lru.Add(key, values[idx], 4)
	}
	for i := len(keys) - 1; i > -1; i-- {
		lru.Get(keys[i])
	}
	next := lru.queue.Front()
	for i := len(keys) - 1; i > -1; i-- {
		assert.Equal(t, i+1, next.Value.(*KeyValuePair).Value)
		next = next.Next()
	}
}

func TestAdd(t *testing.T) {
	defer lru.EvictAll()

	// 16 = 2 * (4 byte key + 4 byte data)
	lru.SetSize(16) // Verify eviction

	// Verify miss
	err := lru.Add("key1", 1, 4)
	assert.Nil(t, err)
	assert.Equal(t, 1, lru.queue.Len())

	// Readd the same key and verify that it did not add another entry
	err = lru.Add("key1", 1, 4)
	assert.Nil(t, err)
	assert.Equal(t, 1, lru.queue.Len())

	// Add new key to verify multiple additions
	err = lru.Add("key2", 2, 4)
	assert.Nil(t, err)
	assert.Equal(t, 2, lru.queue.Len())

	// Ensure that key moves to back of queue if set again
	err = lru.Add("key1", 1, 4)
	assert.Nil(t, err)
	assert.Equal(t, 2, lru.queue.Len())
	assert.Equal(t, 1, lru.queue.Back().Value.(*KeyValuePair).Value)

	// Key 2 should be evicted at this point
	err = lru.Add("key3", 3, 4)
	assert.Nil(t, err)
	assert.Equal(t, 2, lru.queue.Len())
	assert.Equal(t, 3, lru.queue.Back().Value.(*KeyValuePair).Value)
	assert.Equal(t, 1, lru.queue.Front().Value.(*KeyValuePair).Value)
	assert.NotContains(t, lru.cache, "key2")
}

func TestDelete(t *testing.T) {
	defer lru.EvictAll()

	// Verify missing key
	err := lru.Delete("key1")
	assert.Equal(t, cache.ErrKeyNotFound, err)

	lru.Add("key1", 1, 4)
	// Delete key
	lru.Delete("key1")
	assert.Equal(t, 0, lru.queue.Len())
	_, err = lru.Get("key1")
	assert.Equal(t, cache.ErrKeyNotFound, err)
	assert.NotContains(t, lru.cache, "key1")
}

func TestEvict(t *testing.T) {
	defer lru.EvictAll()
	lru.Add("key1", 1, 4)
	lru.Add("key2", 2, 4)
	lru.evict()
	assert.Equal(t, 1, lru.queue.Len())
	assert.NotContains(t, lru.cache, "key1")
}
