package cache

import (
	"container/list"

	"github.com/connorgorman/slack/cache"
)

const defaultCacheSize = 1073741824

// LRUCache is a cache that keeps the most recently used elements in the back
// and will evict the least recently used elements first as it becomes full
type LRUCache struct {
	queue      *list.List               // doubly-linked list to maintain the order of recently used objects
	cache      map[string]*list.Element // Map of string key to the element in the queue
	usage      uint64                   // The amount of cache currently used
	size       uint64                   // The size of the cache in bytes
	cacheStats cache.Stats              // CacheStats to expose for monitoring purposes
}

// KeyValuePair keeps track of the key of a particular value, which is used
// when evicting that element
type KeyValuePair struct {
	Key   string
	Value interface{}
	Size  uint64
}

// NewLRUCache creates and returns a LRUCache
func NewLRUCache() *LRUCache {
	return &LRUCache{
		queue: list.New(),
		cache: make(map[string]*list.Element),
		size:  defaultCacheSize,
	}
}

// Get retrieves the value of the object stored or returns
// ErrKeyNotFound in case the value is not in the cache
func (l *LRUCache) Get(key string) (interface{}, error) {
	elem, ok := l.cache[key]
	if !ok {
		l.cacheStats.Misses++
		return nil, cache.ErrKeyNotFound
	}
	l.cacheStats.Hits++
	l.queue.MoveToBack(elem) // Mark the element as being recently used
	return elem.Value.(*KeyValuePair).Value, nil
}

// Add inserts an element into the cache based on its key
// It will evict the least recently used items to accomodate the new value
func (l *LRUCache) Add(key string, value interface{}, size uint64) error {
	totalSize := uint64(len(key)) + size
	if totalSize > l.size {
		return cache.ErrObjectTooLarge
	}
	if elem, ok := l.cache[key]; ok {
		oldSize := elem.Value.(*KeyValuePair).Size
		elem.Value.(*KeyValuePair).Value = value
		l.queue.MoveToBack(elem)
		l.usage += size - oldSize
		return nil
	}
	for l.usage+totalSize > l.size {
		l.evict()
	}
	l.usage += totalSize
	pair := &KeyValuePair{Key: key, Value: value, Size: size}
	l.queue.PushBack(pair)
	l.cache[key] = l.queue.Back()
	return nil
}

// Delete removes an element from the cache or returns ErrKeyNotFound if its not in the cache
func (l *LRUCache) Delete(key string) error {
	val, ok := l.cache[key]
	if !ok {
		return cache.ErrKeyNotFound
	}
	l.queue.Remove(val)
	delete(l.cache, key)
	l.usage -= (uint64(len(key)) + val.Value.(*KeyValuePair).Size)
	return nil
}

// evict will remove the least recently used element from the cache
func (l *LRUCache) evict() {
	front := l.queue.Front()
	l.Delete(front.Value.(*KeyValuePair).Key)
}

//GetStats returns the cache stats from the current cache
func (l *LRUCache) GetStats() cache.Stats {
	return l.cacheStats
}

// EvictAll clears the cache
func (l *LRUCache) EvictAll() {
	for l.queue.Len() != 0 {
		l.evict()
	}
}

// SetSize will update the size of the cache
// If the size is being set to a smaller value then it will remove elements to match that size
func (l *LRUCache) SetSize(size uint64) {
	l.size = size
	for l.usage > l.size {
		l.evict()
	}
}

func init() {
	cache.Add("lru", func() cache.Cache {
		return NewLRUCache()
	})
}
