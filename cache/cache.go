package cache

// Stats defines the cache stats
type Stats struct {
	Hits   int64
	Misses int64
}

// Cache defines an interface that all caching algorithms must follow to be a backing store for memcache
type Cache interface {
	Get(key string) (interface{}, error)
	Add(key string, value interface{}, size uint64) error
	Delete(key string) error
	EvictAll()
	GetStats() Stats
	SetSize(size uint64)
}

// GetCache allows implementers of the interface this function in an init closure
type GetCache func() Cache

// Caches is the registry of available cache types
var Caches = map[string]Cache{}

// Add adds a cache to the registry through a closure
func Add(name string, getCache GetCache) {
	Caches[name] = getCache()
}
