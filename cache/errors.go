package cache

import (
	"errors"
)

var (
	// ErrKeyNotFound is the error if the element is not in the cache
	ErrKeyNotFound = errors.New("Key Not Found")
	// ErrUnexpectedValue is the error if a compare-and-swap function fails initial validation
	ErrUnexpectedValue = errors.New("Unexpected Value")
	// ErrObjectTooLarge is the error if the object cannot fit in the cache
	ErrObjectTooLarge = errors.New("Object too large")
)
