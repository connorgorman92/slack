VERSION:=1.0.0
SHELL:=/bin/bash
PREFIX:="github.com/connorgorman/slack"

LDFLAGS=-ldflags "-X main.Version=$(VERSION)"

.DEFAULT: memcache

.ALL: memcache-server memcache-client

format:
	(go fmt $(shell go list ./... | grep -v vendor); \
	golint $(shell go list ./... | grep -v vendor))

bin:
	@mkdir -p bin

test:
	go test $(shell go list ./... | grep -v vendor)

memcache-server: format bin
	(cd entrypoint; \
	go build -race $(LDFLAGS) -o ../bin/memcache-server)
	
memcache-client: format bin
	(cd examples/basic; \
	go build -race $(LDFLAGS) -o ../../bin/memcache-client)