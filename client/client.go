package client

import (
	"encoding/binary"
	"fmt"
	"net"
	"sort"
	"sync"

	"github.com/deckarep/golang-set"
	log "github.com/golang/glog"

	"github.com/connorgorman/slack/discovery"
)

// Client defines the memcache client
type Client struct {
	connMap     map[string]net.Conn // connMap is a connection cache for connections based on ip:port
	lock        sync.RWMutex        // lock is used for locking the updates to the server set the client is aware of
	serverSet   mapset.Set          // serverSet is a set of live servers
	serverSlice []string            // serverSlice is a sort set of live servers

	serviceDiscovery discovery.ServiceDiscovery // serviceDiscovery allows for the client to get updates to the available servers
}

// NewClient takes in the name of the service discovery module returns a Client
func NewClient(serviceDiscovery string) (*Client, error) {
	service, ok := discovery.ServiceDiscoveryRegistry[serviceDiscovery]
	if !ok {
		return nil, fmt.Errorf("Service discovery type %s does not exist", serviceDiscovery)
	}
	return &Client{
		connMap:          make(map[string]net.Conn),
		serverSet:        mapset.NewSet(),
		serviceDiscovery: service,
	}, nil
}

// NumServers turns the number of known servers
func (c *Client) NumServers() int {
	return len(c.serverSlice)
}

func (c *Client) getConn(key string) (net.Conn, error) {
	if len(c.serverSlice) == 0 {
		return nil, fmt.Errorf("No memcache servers are available")
	}
	for i := len(key); i <= 2; i++ {
		// Pad the conversion to uint16, which is 65536 which is a reasonable upper limit on memcache instances
		key += "0"
	}
	hashedInt := binary.BigEndian.Uint16([]byte(key))
	idx := hashedInt % uint16(len(c.serverSlice))
	return c.connMap[c.serverSlice[idx]], nil
}

// Initialize sets up the service discovery and launches a go routine to pick up new updates
// It is blocking until the first set of servers are received
// It blocks until the first set of servers is discovered
func (c *Client) Initialize(servers []string) error {
	serverUpdateChan, err := c.serviceDiscovery.WatchServers(servers)
	if err != nil {
		return err
	}
	// Block until first servers are received
	newServerSet := <-serverUpdateChan
	removedSet := c.serverSet.Difference(newServerSet)
	c.generateConnections(newServerSet)
	serverSlice := generateServerSlice(newServerSet)
	c.updateConnections(removedSet, newServerSet, serverSlice)

	go func() {
		newServerSet := <-serverUpdateChan
		removedSet := c.serverSet.Difference(newServerSet)
		c.generateConnections(newServerSet)
		serverSlice := generateServerSlice(newServerSet)
		c.updateConnections(removedSet, newServerSet, serverSlice)
	}()
	return nil
}

// updateConnections is a threadsafe function that updates the server sets and connection maps
func (c *Client) updateConnections(removedSet, newServerSet mapset.Set, newServerSlice []string) {
	c.lock.Lock()
	defer c.lock.Unlock()
	// Close and remove the connections of the servers no longer registered
	for removed := range removedSet.Iter() {
		removedIP := removed.(string)
		c.connMap[removedIP].Close()
		delete(c.connMap, removedIP)
	}
	// We have pre-generated the connections before cutting over to the new set
	c.serverSet = newServerSet
	c.serverSlice = newServerSlice
	log.V(3).Infof("New Set of servers are %+v", newServerSlice)
}

// generateConnections dials the live memcache servers and caches their connections
func (c *Client) generateConnections(newServerSet mapset.Set) {
	addedSet := newServerSet.Difference(c.serverSet)
	for added := range addedSet.Iter() {
		conn, err := net.Dial("tcp", added.(string))
		if err != nil {
			log.Warningf("Failed to connect to %s", added)
			continue
		}
		c.connMap[added.(string)] = conn
	}
}

// generateServerSlice converts the set of servers to a sorted slice based on ip:port
func generateServerSlice(newServerSet mapset.Set) []string {
	var stringSlice []string
	for val := range newServerSet.Iter() {
		stringSlice = append(stringSlice, val.(string))
	}
	sort.Sort(sort.StringSlice(stringSlice))
	return stringSlice
}
