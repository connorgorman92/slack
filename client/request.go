package client

import (
	"bufio"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// StorageRequest is a generic request for an attempt to store an object in memcache
type StorageRequest interface {
	string() string
	getKey() string
	noExpectedReply() bool
}

// TODO discuss data being string (could be blob)

// SetRequest is the request type for a "set" command
type SetRequest struct {
	Key     string
	Flags   uint32
	ExpTime time.Time
	Bytes   uint32
	Data    string
	NoReply bool
}

func (sr *SetRequest) noExpectedReply() bool {
	return sr.NoReply
}

func (sr *SetRequest) string() string {
	baseTemplate := "set %s %d %v %d"
	if sr.NoReply {
		baseTemplate += " noreply"
	}
	baseTemplate += "\r\n%s\r\n"
	return fmt.Sprintf(baseTemplate, sr.Key, sr.Flags, sr.ExpTime.UnixNano(), sr.Bytes, sr.Data)
}

func (sr *SetRequest) getKey() string {
	return sr.Key
}

// GetRequest is the request type for a "get"/"gets" command
type GetRequest struct {
	Keys []string
}

func (gr *GetRequest) fullString() string {
	return fmt.Sprintf("get %s\r\n", strings.Join(gr.Keys, " "))
}

func (gr *GetRequest) string(idx int) string {
	return fmt.Sprintf("get %s\r\n", gr.Keys[idx])
}

// GetResponse is the request type for a "get"/"gets" command
type GetResponse struct {
	Key      string
	Flags    uint32
	Bytes    uint32
	CASValue uint64
	Data     string
}

// NewGetResponse builds a GetResponse from the two expected lines returned by the server
func newGetResponse(line1, line2 string) (*GetResponse, error) {
	splitLine := strings.Split(line1, " ")
	if len(splitLine) != 5 {
		return nil, fmt.Errorf("Incorrect return format from server")
	}
	flags64, err := strconv.ParseInt(splitLine[2], 10, 32)
	if err != nil {
		return nil, err
	}
	bytes64, err := strconv.ParseInt(splitLine[3], 10, 32)
	if err != nil {
		return nil, err
	}
	casValue64, err := strconv.ParseInt(splitLine[4], 10, 64)
	if err != nil {
		return nil, err
	}
	return &GetResponse{
		Key:      splitLine[1],
		Flags:    uint32(flags64),
		Bytes:    uint32(bytes64),
		CASValue: uint64(casValue64),
		Data:     line2,
	}, nil
}

// CASRequest is the request type for a "cas" command
type CASRequest struct {
	*SetRequest
	CASValue uint64
}

func (cr *CASRequest) String() string {
	baseTemplate := "cas %s %d %v %d %d"
	if cr.NoReply {
		baseTemplate += " noreply"
	}
	baseTemplate += "\r\n%s\r\n"
	return fmt.Sprintf(baseTemplate, cr.Key, cr.Flags, cr.ExpTime.UnixNano(), cr.Bytes, cr.CASValue, cr.Data)
}

// DeleteRequest is the request type for a "delete" command
type DeleteRequest struct {
	Key     string
	NoReply bool
}

func (dr *DeleteRequest) String() string {
	baseTemplate := "delete %s"
	if dr.NoReply {
		baseTemplate += " noreply"
	}
	baseTemplate += "\r\n"
	return fmt.Sprintf(baseTemplate, dr.Key)
}

// Retrieve is a thread-safe function for retrieving an object from memcache
func (c *Client) Retrieve(req GetRequest) ([]*GetResponse, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	var responses []*GetResponse
	// TODO(cgorman) look at batching of keys going to similar servers
	for idx, key := range req.Keys {
		conn, err := c.getConn(key)
		if err != nil {
			return responses, err
		}
		if _, err := conn.Write([]byte(req.string(idx))); err != nil {
			return responses, err
		}
		reader := bufio.NewReader(conn)
		if err != nil {
			return responses, err
		}
		line1Bytes, _, err := reader.ReadLine()
		line1 := string(line1Bytes)
		if err != nil {
			return responses, err
		} else if line1 == "END" { // Then there was no key for that value
			continue
		} else if strings.Contains(line1, "ERROR") {
			return responses, errors.New(line1)
		}
		line2, _, err := reader.ReadLine()
		if err != nil {
			return responses, err
		}
		getResponse, err := newGetResponse(line1, string(line2))
		if err != nil {
			return responses, err
		}
		responses = append(responses, getResponse)
	}
	return responses, nil
}

// Store is a thread-safe function for storing an object in memcache
func (c *Client) Store(req StorageRequest) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	conn, err := c.getConn(req.getKey())
	if err != nil {
		return err
	}
	if _, err := conn.Write([]byte(req.string())); err != nil {
		return err
	}
	if req.noExpectedReply() {
		return nil
	}
	reader := bufio.NewReader(conn)
	bytes, _, err := reader.ReadLine()
	if err != nil {
		return err
	}
	if string(bytes) != "STORED" {
		return errors.New(string(bytes))
	}
	return nil
}
