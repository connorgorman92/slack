package zookeeper

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/deckarep/golang-set"
	log "github.com/golang/glog"
	"github.com/samuel/go-zookeeper/zk"

	"github.com/connorgorman/slack/discovery"
)

// Zookeeper struct implements the ServiceDiscovery interface
type Zookeeper struct{}

// Register inserts a caller's IP into the /memcached Znode
func (*Zookeeper) Register(serviceIPs []string, callerIP string) error {
	dd := &discovery.Data{
		IP: callerIP,
	}

	log.V(3).Infof("Connecting to ZK")
	zkConn, _, err := zk.Connect(serviceIPs, 5*time.Second)
	if err != nil {
		return fmt.Errorf("Could not connect to ZK instance: %+v", err)
	}

	log.V(3).Infof("Creating /memcached")
	// For this exercise, I will not worry about Access Control Levels
	_, err = zkConn.Create("/memcached", []byte{}, 0, zk.WorldACL(zk.PermAll))
	if err != nil && err != zk.ErrNodeExists {
		return fmt.Errorf("Failed to create path /memcached. %+v", err)
	}

	log.V(3).Infof("Creating ephemeral node")
	_, err = zkConn.CreateProtectedEphemeralSequential("/memcached/server-", dd.ToJSONBytes(), zk.WorldACL(zk.PermAll))
	if err != nil && err != zk.ErrNodeExists {
		return fmt.Errorf("Failed to create path /memcached. %+v", err)
	}
	return nil
}

// WatchServers returns a channel that will have a value pushed when the set of servers
// participating on the znode change
func (*Zookeeper) WatchServers(zkServers []string) (chan mapset.Set, error) {
	zkConn, _, err := zk.Connect(zkServers, 60*time.Second)
	if err != nil {
		return nil, fmt.Errorf("Could not connect to ZK instances %+v: %+v", zkServers, err)
	}

	if _, _, err := zkConn.Get("/memcached"); err == zk.ErrNoNode {
		return nil, fmt.Errorf("/memcached znode is not created")
	} else if err != nil {
		return nil, fmt.Errorf("Error accessing /memcached znode: %+v", err)
	}
	serverChan := make(chan mapset.Set, 10) // 10 seems to be a reasonable set of changes before going into blocking
	go func() {
		for {
			children, _, memcacheServerChan, err := zkConn.ChildrenW("/memcached")
			if err != nil {
				log.Errorf("Failed to read children of /memcached")
				time.Sleep(5 * time.Second) // Retry in 5 seconds
				continue
			}
			newServerSet := mapset.NewSet()
			for _, child := range children {
				bytes, _, err := zkConn.Get(fmt.Sprintf("/memcached/%s", child))
				if err != nil {
					log.Errorf("Error getting discovery data from %s", child)
					continue
				}
				data := &discovery.Data{}
				if err := json.Unmarshal(bytes, data); err != nil {
					log.Errorf("Failing to unmarshal discovery data. Skipping %s", child)
					continue
				}
				newServerSet.Add(data.IP)
			}
			log.V(3).Infof("New Server Set: %+v", newServerSet)
			serverChan <- newServerSet
			<-memcacheServerChan // Waiting on change in znode children
		}
	}()
	return serverChan, nil
}

// init adds the zookeeper server to the registry of ServiceDiscovery types
func init() {
	discovery.Add("zookeeper", func() discovery.ServiceDiscovery {
		return &Zookeeper{}
	})
}
