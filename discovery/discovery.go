package discovery

import (
	"encoding/json"

	"github.com/deckarep/golang-set"
)

// ServiceDiscovery defines an interface discoverying and registering memcache instances
type ServiceDiscovery interface {
	Register(serviceIPs []string, callerIP string) error
	WatchServers(serverIPs []string) (chan mapset.Set, error)
}

// GetServiceDiscovery allows implementers of the interface to use this function in an init closure
type GetServiceDiscovery func() ServiceDiscovery

// ServiceDiscoveryRegistry defines the map of loaded ServiceDiscovery modules
var ServiceDiscoveryRegistry = map[string]ServiceDiscovery{}

// Add adds a service discovery module to the registry
func Add(name string, getServiceDiscovery GetServiceDiscovery) {
	ServiceDiscoveryRegistry[name] = getServiceDiscovery()
}

// Data is used to store the data related to a memcache instance
type Data struct {
	IP string
}

// ToJSONBytes marshals the data into a []byte
func (dd *Data) ToJSONBytes() []byte {
	bytes, _ := json.Marshal(dd)
	return bytes
}
